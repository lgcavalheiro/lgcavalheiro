---
title: "Ctf Cheat Sheet"
date: "2023-04-03T22:51:08-03:00"
tags: ["cyber-security", "linux", "web"]
tex: false
useNextPrev: false
authors: ["lucas-cavalheiro"]
---

# CTF Cheat Sheet

This is a small cheat sheet I was going to use in a CTF event during my stay at Hurb.

## Login cracking

**Check page source**
: Inspect the page source for commented-out credentials, file paths, hidden inputs, or any other information that might be useful.

<br>

**Manipulate URLs and page source**
: Utilize the browser console to manipulate the URL parameters, input fields, or JavaScript functions to uncover hidden information or exploit vulnerabilities.

<br>

**Explore common website files**
: Look for files like `robots.txt` or `sitemap.xml`, as they may contain valuable clues or hidden paths.

<br>

**Use parsing tools**
: When dealing with encoded or binary data, employ parsing tools like [codebeautify.org](https://codebeautify.org/binary-string-converter) for quick conversions between different formats.

## Useful links

- [codebeautify.org](https://codebeautify.org/binary-string-converter): A general-purpose conversion website that supports various formats such as base64, binary, ROT13, hex, and more.
- [maildrop.cc](https://maildrop.cc/): A service for generating temporary or "burner" email addresses quickly. Useful for CTF challenges that require email verification.
- [hashcat.net](https://hashcat.net/wiki/#howtos_videos_papers_articles_etc_in_the_wild): Hashcat's official wiki with a wealth of information on password cracking techniques and tutorials.
- [unix-ninja.com](https://www.unix-ninja.com/p/A_cheat-sheet_for_password_crackers): A cheat sheet specifically focused on password cracking, providing valuable tips and techniques.
- [RockYou2021.txt](https://github.com/ohmybahgosh/RockYou2021.txt): A popular wordlist commonly used for password cracking.
- [mayfrost/guides](https://github.com/mayfrost/guides/blob/master/ALTERNATIVES.md): A comprehensive list of alternatives to common bloatware software, including sections on pentesting and security tools.
- [osintbrazuca/osint-brazuca](https://github.com/osintbrazuca/osint-brazuca): A curated list of OSINT (Open Source Intelligence) tools specifically focused on the Brazilian context.

## Useful software

- [Hashcat](https://hashcat.net/hashcat/): An advanced password recovery tool that supports various hashing algorithms and attack modes.
- [OWASP ZAP](https://www.zaproxy.org/): An automated vulnerability scanner designed to identify security flaws in web applications.

## Sources

Here are some other assorted links I used as sources:

- [zacheller.dev](https://zacheller.dev/DTW-intro): A website providing solutions and explanations for DefendTheWeb (DTW) introductory challenges.
