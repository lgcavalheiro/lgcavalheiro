---
title: "Setting Up a Linux Server - Part 2"
date: "2023-05-04T20:30:22-03:00"
tags: ["linux", "shellscript", "python"]
tex: false
useNextPrev: true
authors: ["lucas-cavalheiro"]
---

# Setting Up a Linux Server - Part 2

- [Introduction](#introduction)
- [Installing and configuring unattended-upgrades](#installing-and-configuring-unattended-upgrades)
- [Installing and configuring fail2ban](#installing-and-configuring-fail2ban)
  - [Configuring jail.local](#configuring-jaillocal)

## Introduction

Unattended-upgrades is used to automatically upgrade packages on your server, fail2ban will ban IPs that keep hitting the server, which is useful for blocking bots.

## Installing and configuring unattended-upgrades

To install and enable unattended-upgrades, run the following commands:

```sh
apt install unattended-upgrades
systemctl enable unattended-upgrades
```

Configs for unattended-upgrades are stored in `/etc/apt/apt.conf.d/50unattended-upgrades` and `/etc/apt/apt.conf.d/20auto-upgrades`, if configs are changed, do a dry run to confirm unattended-upgrades was configured correctly, you can use this command to dry run: `unattended-upgrades --dry-run --debug`.

## Installing and configuring fail2ban

Make sure timezone data is correct, or fail2ban may fail to ban (haha) the IPs. Use the commands `dpkg-reconfigure tzdata` or `date` to view the current system timezone, if you need to reconfigure the timezone, return to [New server checklist]({{< relref "/tutorials/setting-up-a-linux-server-part-1.md#new-server-checklist" >}}) and rerun the timezone configuration command.

[//]: # "TODO: weird out of place rsyslog bit, check where this goes later"

```sh
systemctl restart rsyslog && systemctl status rsyslog
```

The daemon `rsyslog` is the one that logs everything on a server, usually at `/var/log`, fail2ban has a log file named `fail2ban.log` and ssh has `auth.log`, which is used for logging ssh connection events.

To install fail2ban, run `apt install fail2ban`. Fail2ban config files are located on `/etc/fail2ban`, named `fail2ban.conf` and `jail.conf`. We'll need to make some copies of these files now:

```sh
cp jail.conf jail.local && cp fail2ban.conf fail2ban.local
```

We should edit only the `.local` files from now on. We do this, so in the off chance we have to reinstall fail2ban, we don’t lose our custom configurations by having the `.conf` files overwritten during the reinstall process.

### Configuring jail.local

The `[DEFAULT]` category contains configs used for all services. We will perform our configurations under this category.

1. Uncomment `ignoreip = 127.0.0.1/8 ::1`.
2. Set `bantime` to a time period of your choosing. You can set it to `-1` for permanent bans. Example: `1h` (1 hour).
3. Set `findtime` to a time period of your choosing. This is the time limit for retries. If someone exceeds their retries within this time period, they will be banned. Example: `20m` (20 minutes).
4. Set `maxretry` to the maximum number of allowed retries before Fail2ban attempts to ban the IP. Example: `3`.

The following configurations will be under the `[sshd]` category:

1. Set `mode` to `normal`.
2. Above the `mode` line, add `enabled = true` to enable Fail2ban for SSH.
3. Set the port to whatever port you are using for SSH. By default, Fail2ban targets port 22, so if you are using a different port, you should change the `port` config. You should also set this port config on categories `[dropbear]` and `[selinux=ssh]`.

You can configure different settings for different services by changing their configurations under different categories. Each category represents a service (except for `[DEFAULT]`, of course). These are some basic categories that tend to be used by most services and are used here for educational purposes. Keep in mind that your server needs will be different from other admins, so configure Fail2ban according to your needs and avoid copying configs from others if possible.

Save these configs and then enable the Fail2ban service with `systemctl enable fail2ban && systemctl start fail2ban && systemctl status fail2ban`. You can also check if Fail2ban is working by using `cat /var/log/fail2ban.log`. Whenever you change Fail2ban configs, restart the service with `systemctl restart fail2ban` so the changes can take effect.

You can set different profiles for your service configs inside directory `jail.d`. This can be done by creating a file inside `jail.d` named `<CATEGORY_NAME>.local`, for example `sshd.local` for the `[sshd]` category.
