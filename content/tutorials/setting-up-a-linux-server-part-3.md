---
title: "Setting Up a Linux Server - Part 3"
date: "2023-05-12T21:05:42-03:00"
tags: ["linux", "shellscript"]
tex: false
useNextPrev: true
authors: ["lucas-cavalheiro"]
---

# Setting Up a Linux Server - Part 3

- [Firewall and ufw](#firewall-and-ufw)
  - [What is a firewall?](#what-is-a-firewall)
  - [Installing and configuring a firewall](#installing-and-configuring-a-firewall)

## Firewall and ufw

### What is a firewall?

Simply put, a firewall is used to protect either a single machine or a whole network against external attacks. You can think of a firewall as acting as a "receptionist" between the internet and your network. This is accomplished by setting firewall rules that can block one or more network ports and specifying if the connections to be blocked on these ports are incoming, outgoing or both. You can also specify different rules for different IP addresses.

- Incoming connection: Every connection "entering" the network (from internet to network)
- Outgoing connection: Every connection "exiting" the network (from network to internet)

### Installing and configuring a firewall

Standard firewall on Linux is done with netfilter, which is already embedded in the Linux kernel. Usually, netfilter is used via iptables for better ease of use. We can use ufw, which will make things even easier. There's also gufw, which is just ufw with a graphical user interface. We'll just use ufw, since we're on a server without graphical support. Note that it is better to toy with ufw for the purposes of learning using a virtual machine, toying with ufw directly on a server that you do not have physical access to may lead you to lock yourself out of your own server!

Here's a table containing the very basics in terms of ufw commands to get you started on configuring your firewall:

| Description                                          | Command                                         | Example                                                 |
| ---------------------------------------------------- | ----------------------------------------------- | ------------------------------------------------------- |
| Install ufw                                          | `apt install ufw`                               | -                                                       |
| Check ufw status and configurations                  | `ufw status`                                    | -                                                       |
| Set default behaviors                                | `ufw default <BEHAVIOR>`                        | `ufw default deny outgoing` `ufw default deny incoming` |
| Reload ufw policies                                  | `ufw reload`                                    | -                                                       |
| Enable the firewall and enable on boot               | `ufw enable`                                    | -                                                       |
| Enable all traffic on a single port                  | `ufw allow <PORT_NUMBER>`                       | `ufw allow 80`                                          |
| Check rules with numbers                             | `ufw status numbered`                           | -                                                       |
| Delete a rule                                        | `ufw delete <RULE_NUMBER>`                      | -                                                       |
| Reset ufw                                            | `ufw reset`                                     | -                                                       |
| Enable only outgoing traffic on a port               | `ufw allow out <PORT_NUMBER>`                   | `ufw allow out 443` or `ufw allow out https`            |
| List all applications that have integration with ufw | `ufw app list`                                  | -                                                       |
| Allow incoming traffic                               | `ufw allow in <PORT_NUMBER>`                    | `ufw allow in 80`                                       |
| Allow traffic on a port for specific IP addresses    | `ufw allow from <IP> to any port <PORT_NUMBER>` | `ufw allow from 192.168.1.100 to any port 22`           |

Now that we know some of the basic ufw commands, we will go through the steps required to secure our server.

1. First, we will deny all outgoing traffic by running the command `ufw default deny outgoing`. This will ensure that no outgoing traffic can leave the server without being explicitly allowed.

2. Next, we will deny all incoming traffic by running the command `ufw default deny incoming`. This will prevent any unauthorized access to the server from outside.

3. Now, we can start allowing traffic on a per-port basis. To allow incoming traffic on a specific port, use the command `ufw allow <PORT_NUMBER>`. For example, to allow incoming traffic on port 80 (HTTP), use the command `ufw allow 80`. You'll need to do this for every port that needs to have incoming traffic, remember your custom SSH port? ;)

4. Similarly, to allow outgoing traffic on a specific port, use the command `ufw allow out <PORT_NUMBER>`. For example, to allow outgoing traffic on port 443 (HTTPS), use the command `ufw allow out 443`.

5. It is important to carefully review all the rules that have been created so far. You can use the command `ufw status numbered` to view all the rules along with their numbers.

6. Once you are absolutely sure that the rules are correct and will not cause any unintended consequences, you can enable ufw by running the command `ufw enable`. This will activate the firewall and ensure that it starts automatically every time the server boots up.

After executing the aforementioned steps, you have effectively secured your server using ufw!
