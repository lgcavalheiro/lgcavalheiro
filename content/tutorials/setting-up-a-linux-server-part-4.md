---
title: "Setting Up a Linux Server - Part 4"
date: "2023-05-19T19:45:17-03:00"
tags: ["linux", "shellscript", "python", "apache"]
tex: false
useNextPrev: true
authors: ["lucas-cavalheiro"]
---

# Setting Up a Linux Server - Part 4

- [Apache, Domains, and Certificates](#apache-domains-and-certificates)
  - [Apache](#apache)
    - [Configuring Apache Security](#configuring-apache-security)
      - [In security.conf](#in-securityconf)
      - [In apache2.conf](#in-apache2conf)
  - [Configuring domains](#configuring-domains)
  - [Configuring Certificates with Certbot and Apache](#configuring-certificates-with-certbot-and-apache)

## Apache, Domains, and Certificates

### Apache

Apache is an open-source web server that allows you to serve websites over the internet. It is capable of serving content through the web in a variety of ways, including static HTML pages and dynamic PHP scripts, for example. Apache can also have its functionality extended via modules. It is widely used and established in the market, with reliable stability, performance and security. When it comes to serving websites, Apache uses a technique called **virtual hosting** that enables hosting multiple websites on a single Apache instance.

#### Configuring Apache Security

Below are some of the critical configuration files and folders that Apache uses:

| Configuration File/Folder                   | Description                                                                                                                                                 |
| ------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/var/www/html/`                            | This folder is where the static website files are stored.                                                                                                   |
| `/etc/apache2/conf-available/security.conf` | The security configurations for Apache are located in this file.                                                                                            |
| `/etc/apache2/apache2.conf`                 | The general configuration file for Apache can be found here.                                                                                                |
| `/etc/apache2/envvars`                      | Environment variables used by Apache are stored in this file.                                                                                               |
| `/etc/apache2/mods-enabled/`                | This folder contains the active modules that are currently in use by Apache. You can disable a module by removing it's corresponding file from this folder. |

By understanding the different configuration files and folders Apache uses, we can better manage and customize our Apache instance for our web hosting needs.

Now, we will begin to configure our Apache server for better security. These configurations are stored in two files: `security.conf` and `apache2.conf`.

##### In `security.conf`

- Set the `ServerTokens` to `Prod` to make the server return only the essential information in the HTTP header.
- Set `ServerSignature` to `Off` to prevent Apache from displaying version information.

##### In `apache2.conf`

- Under `<Directory /var/www/>` remove the line `Options Indexes FollowSymLinks` to prevent directory listing.
- Under `<Directory /var/www/>` add the line `Options -ExecCGI` to remove CGI executions.
- Under `<Directory /var/www/>` add the line `Options -Indexes` to remove directory listings.
- Under `<Directory /var/www/>` add the line `Options -FollowSymLinks` to remove directory listings for symlinks.
- Set `Timeout` to `45` to close connections after 45 seconds of inactivity.
- Set `MaxKeepAliveRequests` to `50` to limit the number of requests allowed to a single persistent connection.

## Configuring domains

To configure domains on your server, you need to add the domain name to your `/etc/hostname` file. If you prefer, you can use the command `hostnamectl set-hostname <DOMAIN>` to set the hostname. However, you should also configure the domain on your registrar's website. Your registrar is the company where you purchased your domain name. It is recommended to consult their documentation or support for instructions on configuring the DNS settings for your domain. I personally prefer to go with the registrar method only.

## Configuring Certificates with Certbot and Apache

To secure your website with HTTPS, you need to install an SSL certificate. You can either buy a certificate from a company or generate your own using Certbot. If you use Certbot, you will be the one signing the certificate.

To use Certbot with Apache, you first need to install it using the command `apt install python3-certbot-apache`. Then, to generate a certificate for your domain, run `certbot --apache` in your terminal. Certbot will ask you for the following information:

- Email: This is the email address that Certbot will use to send warnings to the system administrator. If you don't want to provide an email address, you can use the `--register-unsafely-without-email` flag.
- Domain Names: Enter the domain names that you want to secure with HTTPS, separated by commas.
- Redirection: Certbot will ask you whether you want to redirect all traffic to use HTTPS. It is recommended that you choose to do so.

Finally, you should set up a cron job to automatically renew your certificate before it expires. You can do this by running `sudo crontab -e` and adding the following line to the crontab file:

```
0 12 * * * /usr/bin/certbot renew --quiet
```

This will run the Certbot renewal process every day at noon.

## Conclusion

After going through all the parts of this tutorial series, we have gained a better understanding on how to manage and secure a linux server and on how to host a website using it, with encryption enabled and using a custom domain name.

This was a good starting point into the world of linux servers, but there's a vast array of possibilities that surpass the scope of this series. It's now your responsibility to go out there and keep on learning, equipping yourself with the necessary skills to overcome any challenges that you may face. I sincerely appreciate your dedication in following this tutorial series, thank you for your readership, and until next time!
