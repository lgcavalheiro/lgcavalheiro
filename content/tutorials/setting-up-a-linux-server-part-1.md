---
title: "Setting Up a Linux Server - Part 1"
date: "2023-04-27T18:42:11-03:00"
tags: ["linux", "shellscript"]
tex: false
useNextPrev: true
authors: ["lucas-cavalheiro"]
---

# Setting Up a Linux Server - Part 1

- [Introduction](#introduction)
- [A small primer before we start](#a-small-primer-before-we-start)
- [New server checklist](#new-server-checklist)
- [Using SSH](#using-ssh)
  - [Initial security SSH configs](#initial-security-ssh-configs)

## Introduction

This tutorial series discusses the _how_ of things and not the _why_, so it is expected of the reader to already know _why_ they want to setup their own linux server and would just like to know _how_ to do that, we'll go over the very practical basics of doing that in the following sections.

The only prerequisites for following these instructions are:

- Having a PC with working internet.
- A linux server already up and running, this can be a VPS or a local machine, i'll be using a VPS running Debian 10 for this article, but as long as it is a linux server and you can SSH into it, you'll be good to go.

## A small primer before we start

Here's some concepts that i would like you, as the reader, to know before we proceed.

**VPS**
: A Virtual Private Server is a virtual machine that runs on a physical server and provides users with dedicated resources and greater control over their server configuration. With a VPS, users can install and run their own operating system, software applications, and have root access to the server. This allows users to have greater flexibility and scalability in hosting their websites or applications.

<br>

**Shared hosting**
: On the other hand, shared hosting is a hosting service where multiple users share the same physical server and its resources. This type of hosting is typically more affordable, but it also has limitations such as limited resources and less control over the server configuration.

<br>

**SSH**
: Secure Shell is a protocol used for securely accessing a remote server over the internet. It provides a secure encrypted connection between the client and server, allowing users to execute commands on the remote server as if they were sitting in front of it. SSH is commonly used by system administrators and developers to remotely manage servers, transfer files, and execute commands. It is widely used due to its security, flexibility, and ease of use.

## New server checklist

Before working on hardening our server's security, we'll need to go through the following steps:

- [x] Update the operating system: `apt update && apt upgrade`
- [x] Set the system timezone: `timedatectl set-timezone <TZ>`
- [x] Change the root password: `passwd`
- [x] Create a non-admin user: `adduser <USERNAME>`

## Using SSH

To access SSH use `ssh <USER>@<SERVER_ADDRESS>`, connect fingerprint and then provide the user password, you should be connected to your server from your work machine via SSH after this.

NOTE: Your server should **NEVER** have root SSH and default SSH port (22) active in production!
Later on in this article, we will go through on how to solve this.

### Initial security SSH configs

Here are some initial security SSH configs that can be used to secure your server. These configs can be found in the `/etc/ssh/sshd_config` file:

| Configuration Type                           | Explanation                                                                                                                                                                                                             | Example Configuration     |
| :------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------ |
| Change standard SSH port                     | The default port for SSH is 22, and it is recommended to change it to another port number to reduce the chances of attacks. Change config `Port 22` to another port number.                                             | `Port 2222`               |
| Turn off SSH root login                      | Disabling root login for SSH is a good security practice since attackers often target the root user account. In the config file, that is defined on the line `PermitRootLogin yes`.                                     | `PermitRootLogin no`      |
| Turn off login attempts with empty passwords | Allowing login attempts with empty passwords is a major security risk and should be disabled. This is defined by the line `PermitEmptyPasswords yes/no` which may not be present in the file, so you'll have to add it. | `PermitEmptyPasswords no` |
| Turn off X11 Forwarding                      | X11 Forwarding can be a potential security risk, and it's better to turn it off if it's not needed. This is defined on the line `X11Forwarding yes/no`.                                                                 | `X11Forwarding no`        |
| Turn on idle timeout                         | Setting up an idle timeout can be useful in preventing idle SSH sessions from being left open indefinitely. Add `ClientAliveInterval <INTERVAL_IN_SECONDS>`.                                                            | `ClientAliveInterval 300` |
| Turn on max login attempts                   | Setting up a maximum number of authentication attempts can help prevent brute force attacks. Add `MaxAuthTries <TRY_NUMBER>`.                                                                                           | `MaxAuthTries 3`          |
| Turn on max sessions per IP                  | Limiting the number of sessions per IP can help prevent Denial of Service (DoS) attacks. Add `MaxSessions <SESSIONS_NUMBER>`.                                                                                           | `MaxSessions 2`           |

**Note 1:** run `systemctl restart ssh` after you finish writing your configs.  
**Note 2:** to view users and session program, use `cat /etc/passwd`.
