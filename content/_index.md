---
title: "Home"
date: "2023-05-31T21:11:03-03:00"
---

# Lucas' Personal Webpage

Hey there! Welcome to my website! I'm Lucas Cavalheiro, a passionate professional in the realm of Software Development. 🚀

## About Me

I've been tinkering with technology for as long as I can remember, and it's become more than just a hobby for me. I'm all about creating innovative solutions and pushing the boundaries of what's possible to achieve with technology in people's every day life. Whether it's creating software or crafting simple yet effective inventions, I thrive on the excitement of turning ideas and plans into reality!

I'm well-versed in various programming languages like Typescript, Python and Go, all used within the liberating environment of a GNU/Linux distro. With my strong analytical skills and ability to think outside the box, I can tackle any coding challenge that comes my way. If you can name it, i can create it!

Feel free to peruse the latest content on my website by checking out the cards at the bottom if this page, also, feel free to check out my other links:

- [Gitlab](https://gitlab.com/lgcavalheiro)
- [Github](https://github.com/lgcavalheiro)
- [LinkedIn](https://www.linkedin.com/in/lgcavalheiro/)
